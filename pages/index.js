import { Component } from 'react';

import { Button, About, Header, Timeline } from 'components';

import { getQrCode, awaitStatus } from 'utils/sockets';

class MobSpace extends Component {
  state = {
    sectionIndex: 0,
    name: '',
    qrCode: '',
    signedIn: false,
    showAbout: false,
  };

  nextSection = () =>
    this.setState({ sectionIndex: this.state.sectionIndex + 1 });
  handleShowAbout = () => this.setState({ showAbout: true });
  handleHideAbout = () => this.setState({ showAbout: false });

  handleSSO = async() => {
    try {
      const { qrCode, socket, identifier } = await getQrCode('authenticate');

      this.setState({ qrCode });
      this.nextSection();

      const data = await awaitStatus({
        socket,
        identifier,
      });

      const { status, givenName } = data;
      if (status === 'success') {
        this.setState({ signedIn: true, name: givenName });
        this.nextSection();
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const sections = [
      <main className="Landing">
        <h1>
          Someone nearby <br />
          has a car for you
        </h1>
        <p>
          A fully decentralised peer-to-peer car sharing. No more paperwork,
          Smart contracts will handle that for you.
        </p>
        <br />
        <Button onClick={this.nextSection}>Become a member</Button>
        <style jsx>{`
          h1 {
            margin-top: 0;
          }
        `}</style>
      </main>,
      <div className="Content ta-c">
        <img src="/static/images/aelondo_black.png" alt="" width={180} />
        <h2 style={{ width: '16.67rem' }}>Sign up as a new member</h2>
        <img className="SocialButton" src="/static/images/fb_button.svg" />
        <img className="SocialButton" src="/static/images/google_button.svg" />
        <h5>OR</h5>
        <Button withLogo className="mt" onClick={this.handleSSO}>
          Continue with Jolocom
        </Button>
        <div style={{ marginTop: 'auto' }}>
          <h4>Already a member?</h4>{' '}
          <h4 style={{ cursor: 'pointer' }}>Log in</h4>
        </div>

        <style jsx>{`
          .SocialButton {
            width: 250px;
            display: block;
            margin: auto;
            margin-top: 0.83rem;
          }
        `}</style>
      </div>,
      <div className="Content ta-c">
        <img src="/static/images/aelondo_black.png" alt="" width={180} />
        <h2 style={{ width: '16.67rem' }}>Sign up as a new member</h2>
        <h3>Scan the QR-code with your SmartWallet:</h3>
        <img src={this.state.qrCode} className="qrCode" />
      </div>,
      <div className="Summary ta-c">
        <div className="Content">
          <img src="/static/images/aelondo_black.png" alt="" width={180} />
          <h2>Welcome, {this.state.name}!</h2>
          <p className="gray" style={{ width: '41.83rem' }}>
            Just imagine… <br />
            If this were a real P2P car-sharing service, you would be all set to
            browse hundreds of cars available to rent nearby, make a reservation
            using a smart contract, unlock a car with your SmartWallet and drive
            off — simple, seamless, and secure.
            <br />
            <br />
            For now, the demo concludes here.
          </p>

          <Button
            flat
            pink
            style={{ marginTop: '3.25rem' }}
            onClick={this.handleShowAbout}
          >
            About this experience
          </Button>
        </div>

        <style jsx>{`
          .Summary .Content {
            flex: initial;
          }
        `}</style>
      </div>
    ];

    const currentSection = sections[this.state.sectionIndex];
    return (
      <div className="MobSpace">
        <Header
          handleShowAbout={this.handleShowAbout}
          brandVersion={this.state.sectionIndex === 0 ? 'primary' : 'secondary'}
        />

        <div style={{ display: 'flex' }}>
          <Timeline />

          {typeof currentSection === 'function'
            ? React.createElement(currentSection, this.props)
            : currentSection}
        </div>

        {this.state.showAbout && <About onClose={this.handleHideAbout} />}

        <style jsx>{`
          .MobSpace {
            display: flex;
            flex-direction: column;
            background-image: url('/static/images/MOB_bg.jpg');
            background-position: center;
            background-repeat: no-repeat;
            background-color: #000;
            background-size: cover;
            padding: 5.42rem;
            height: 100vh;
            overflow: hidden;
          }
          .MobSpace :global(.Landing) {
            flex: 1;
            padding: 12.8rem 0 0 6.9133333333rem;
            overflow-y: auto;
            width: 50rem;
          }
          .MobSpace :global(.Landing h1) {
            margin-bottom: 0;
          }
          .MobSpace :global(.Landing p) {
            width: 28.5rem;
          }

          .MobSpace :global(.Timeline) {
            margin-left: 10px;
            margin-top: 3.67rem;
          }

          .MobSpace :global(.Content) {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 2.5rem 1rem 1.67rem;
            background-color: #fff;
            color: #05050d;
            margin: auto;
            overflow-y: auto;
            width: 60.5rem;
            height: 44.17rem;
          }

          .MobSpace :global(.Summary) {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-image: url('/static/images/MOB_final_bg.jpg');
            background-repeat: no-repeat;
            background-size: cover;
            display: flex;
            align-items: center;
            z-index: 10;
          }

          @media only screen and (min-width: 1440px) {
            .MobSpace :global(.Content) {
              height: 45.83rem;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default MobSpace;
