const Timeline = () => (
  <div className="Timeline">
    <div className="Glow" />

    <style jsx>{`
      .Timeline {
        height: 55rem;
        width: 7px;
        background: url('/static/images/timeline.svg');
        background-repeat: no-repeat;
      }
      img {
        height: 100%;
      }
      .Glow {
        position: fixed;
        bottom: 1.67rem;
        background-image: url('/static/images/glow-checkbox.svg');
        background-size: cover;
        width: 5.92rem;
        height: 5.92rem;
        border-radius: 50%;
        transform: translate(-45%, 0%);
        z-index: 2;
      }
    `}</style>
  </div>
);

export default Timeline;
