import SVG from 'react-inlinesvg';

const Header = ({ brandVersion = 'primary', handleShowAbout }) => {
  const isPrimaryBrandVersion = brandVersion === 'primary';

  return (
    <header>
      <SVG className="Logo" src="/static/images/jolocom-icon-transparent.svg">
        <img src={'/static/images/jolocom-icon-transparent.svg'} alt="Jolocom's logo" />
      </SVG>
      {isPrimaryBrandVersion ? (
        <img
          src="/static/images/aelondo_white.png"
          alt="Aelondo logo"
          className="Brand"
          width={180}
        />
      ) : (
        <span className="Brand" onClick={handleShowAbout}>
          About
        </span>
      )}

      <style jsx>{`
        header {
          position: relative;
          display: inline-flex;
          align-items: center;
          height: 3.33rem;
          z-index: 1;
        }
        header :global(.Logo svg .image) {
          opacity: 0.285673;
        }
        header:hover .Brand {
          visibility: visible;
          color: ${isPrimaryBrandVersion ? '' : '#fff'};
        }
        header:hover :global(.Logo svg .image) {
          opacity: ${isPrimaryBrandVersion ? 0.285673 : 1};
        }
        img.Brand {
          margin-left: 4.5rem;
        }
        .Brand {
          margin-left: 20px;
          text-transform: uppercase;
          visibility: ${isPrimaryBrandVersion ? 'visible' : 'hidden'};
          cursor: ${isPrimaryBrandVersion ? 'default' : 'pointer'};
          color: rgba(255, 255, 255, 0.4);
          font-size: 1rem;
          line-height: 12px;
          letter-spacing: 2.6px;
        }
        .Brand a {
          color: rgba(255,255,255,0.4);
          transition: color .4s;
          text-decoration: none;
        }
        .Brand a:hover {
          color: #fff;
        }
      `}</style>
    </header>
  );
};

export default Header;
